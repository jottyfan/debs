add this to the /etc/apt/sources.list.d/github.jottyfan.list file:
```bash
deb https://www.gitlab.com/jottyfan/debs-/raw/main bullseye main
```

add the `apt.key` file with the command
```bash
sudo apt-key add apt.key
```

